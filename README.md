Exercice
=

This project is meant as an exercice for IT students.

Project Goals
==
Sorting an array is a common exercise.  Even though a developper no longer needs to sort arrays as many methods already exist. 
It is a very good practice to train on realizing them. 

On one side, you shall train your brain to deal with some coding subtilities. 
But also, on another side, these algorythms are used by companies as 'hiring' exercices. So you'd better know how they work!


Installation
==
How to install it ? Nothing easier. 
First, you have to have python  installed on your computer. 

Then launch : `pip install -r requirements.txt`.

Explanation
==
This project is implemented the following way :
 - **src/sort_test.py** : Tests written. You should not modify these tests. 
 - **src/sort.py** : The file you have to produce. For each method, a docstring is written in order to explain the logic required. 

 Running the tests
 ==
 
 There are several ways for executing your tests :
  - **With pycharm**, you can simply open the test file and click on the green arrow button shown on the left side of each test.
  - **With your terminal**, you can type `cd src/;pytest -v`

Aller plus loin
==
Avez-vous déjà entendu parler du PEP? Cette convention sur les standards à appliquer lorsqu'on implémente des scripts en python ?
Vous avez au sein de ce dépot, un linter permettant de peaufiner votre code afin de s'approcher au plus près des conventions attendues. 

Pour activer le linter et voir ce que vous devez réaliser, appelez la méthode : 

`pylint src/*.py` 